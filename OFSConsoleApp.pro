QT += core network
QT -= gui

TARGET = OFSConsoleApp
CONFIG += console
CONFIG -= app_bundle

DEFINES -= OFS_LIBRARY

INCLUDEPATH += $$PWD

LIBS += -L$$PWD

win32 {
}
unix {
}

CONFIG(debug, debug|release) {
    LIBS += -lOFSd
} else {
    LIBS += -lOFS
}

TEMPLATE = app

SOURCES += main.cpp \
    processor.cpp

HEADERS += \
    processor.h \
    ofs.h \
    ofsdom.h

DISTFILES += \
    OFS.lib \
    OFSd.lib \
    OFS.dll \
    OFSd.dll \
    libOFS.so \
    libOFS.so.1 \
    libOFS.so.1.0 \
    libOFS.so.1.0.0 \
    libOFSd.so \
    libOFSd.so.1 \
    libOFSd.so.1.0 \
    libOFSd.so.1.0.0 \
    OFSConsoleApp.pro.user
