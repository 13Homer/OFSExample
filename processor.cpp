#include "processor.h"
#include <QObject>
#include <QDebug>
#include <QDateTime>

/* Instrumenty (id, nazwa, minimalna zmiana ceny czyli tickSize):
 *   1 : EURUSD          0.00005
 *   3 : S&P500          0.25
 *   4 : CrudeOil        0.01
 *   5 : 10 year bonds   0.015625
 */

const bool SHOW_TICKS = true;
const bool SHOW_DOM = true;

void Processor::tick(int id, int iprice, int bids, int asks, double tickSize, const QDateTime &timestamp) {
    if (!SHOW_TICKS) return;

    qDebug() << id << timestamp.toString("yyyy-MM-dd HH:mm:ss").toStdString().c_str() << "price:" << (iprice * tickSize) << "buy:" << bids << "sell:" << asks << "(" << "iprice=" << iprice << "tickSize:" << tickSize << ")";
}

void Processor::depthOfMarket(int id, double tickSize, const domlevels_t &doms) {
    if (!SHOW_DOM) return;

    typedef QMapIterator<int, OFSDepthOfMarketLevel*> iterator_t;
    iterator_t it(doms);

    switch (id) {
    case 1: qDebug() << "EURUSD"; break;
    case 3: qDebug() << "S&P500"; break;
    case 4: qDebug() << "CrudeOil"; break;
    case 5: qDebug() << "10y Bonds"; break;
    }

    qDebug() << "SELL:";
    it.toBack();
    while (it.hasPrevious()) {
        it.previous();
        const OFSDepthOfMarketLevel *level = it.value();
        qDebug() << " " <<level->iprice_ask*tickSize<<"x"<<level->asks;
    }

    qDebug() << "BUY:";
    it.toFront();
    while (it.hasNext()) {
        it.next();
        const OFSDepthOfMarketLevel *level = it.value();
        qDebug() << " " << level->iprice_bid*tickSize <<"x"<<level->bids;
    }
    qDebug() << "";
}
