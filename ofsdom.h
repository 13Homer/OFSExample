#ifndef OFSDOM_H
#define OFSDOM_H

#include <QMap>

class Q_DECL_IMPORT OFSDepthOfMarketLevel {
public:
    const int iprice_bid, iprice_ask, bids, asks;

    OFSDepthOfMarketLevel(int iprice_bid, int iprice_ask, int bids, int asks);
};

typedef QMap<int, OFSDepthOfMarketLevel*> domlevels_t;

#endif // OFSDOM_H
