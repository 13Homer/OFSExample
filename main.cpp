#include "ofs.h"
#include "processor.h"
#include <QCoreApplication>
#include <QDebug>

int main(int argc, char *argv[]) {
    QCoreApplication a(argc, argv);

    OFS ofs;
    Processor processor;
    QObject::connect(&ofs, SIGNAL(tick(int,int,int,int,double,const QDateTime&)), &processor, SLOT(tick(int,int,int,int,double,const QDateTime&)));
    QObject::connect(&ofs, SIGNAL(depthOfMarket(int,double,domlevels_t)), &processor, SLOT(depthOfMarket(int,double,domlevels_t)));
    ofs.start();

    a.exec();
}
