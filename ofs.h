#ifndef OFS_H
#define OFS_H

#include "ofsdom.h"
#include <QObject>

class Connector;

class Q_DECL_IMPORT OFS : public QObject {
    Q_OBJECT
    Connector *connector;
public:
    OFS();
    void start();
    void stop();
private slots:
    void _tick(int id, int iprice, int bids, int asks, double tickSize, const QDateTime &timestamp);
    void _dom(int id, double tickSize, const domlevels_t &doms);
signals:
    void tick(int id, int iprice, int bids, int asks, double tickSize, const QDateTime &timestamp);
    void depthOfMarket(int id, double tickSize, const domlevels_t &doms);
};
\
#endif // OFS_H
