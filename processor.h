#ifndef PROCESSOR_H
#define PROCESSOR_H

#include "ofsdom.h"
#include <QObject>

class Processor : public QObject {
    Q_OBJECT
private slots:
    void tick(int id, int iprice, int bids, int asks, double tickSize, const QDateTime &timestamp);
    void depthOfMarket(int id, double tickSize, const domlevels_t &doms);
};

#endif // PROCESSOR_H
